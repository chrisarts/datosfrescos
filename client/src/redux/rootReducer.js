import { combineReducers } from 'redux';
import flashMessages from './reducers/flashMessages';

//Mix the reducers for this app in the localStorage
export default combineReducers({
    flashMessages
});
