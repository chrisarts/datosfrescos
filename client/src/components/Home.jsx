import React from 'react';

import Canvas from './canvas/Canvas';
import CanvasSize from './canvas/CanvasSize';

class Home extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            width: 500,
            height: 300
        };
    }

    render(){
        return(
            <div className="page home">
                <h1>Crear Lienzo</h1>
                <Canvas
                    width={this.state.width}
                    height={this.state.height}
                />
            </div>
        )
    }
}

export default Home;