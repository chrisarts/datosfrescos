import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';

class CanvasSize extends React.Component{

    constructor(props){
        super(props);
        this.styles = {
            textField: {
                width: '30%',
                input: {
                    textAlign: 'center'
                }
            }
        }
    }

    render(){
        return(
            <form className="canvas-size">
                <TextField
                    name="width"
                    hintText="En Pixeles"
                    floatingLabelText="Ancho del Lienzo"
                    floatingLabelFixed={true}
                    onChange={this.props.onChange.bind(this)}
                    value={this.props.width}
                    type="number"
                    inputStyle={this.styles.textField}
                />
                <span>X</span>
                <TextField
                    name="height"
                    hintText="En Pixeles"
                    floatingLabelText="Alto del Lienzo"
                    onChange={this.props.onChange.bind(this)}
                    value={this.props.height}
                    type="number"
                    inputStyle={this.styles.textField}
                />
            </form>
        )
    }
}

CanvasSize.PropTypes = {
    onChange: PropTypes.func.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired
};

export default CanvasSize;