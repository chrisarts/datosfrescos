import React from 'react';
import PropTypes from 'prop-types';
import {
    EditorFormatSize,
    ImageFilter,
    HardwareDeveloperBoard
} from 'material-ui/svg-icons';
import IconButton from 'material-ui/IconButton';

class CanvasToolbox extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            text: {
                id: -1,
                type: 'text',
                screenX: 20,
                screenY: 30,
                content: 'Area de Texto',
                areaConfig: {
                    fontFamily: 'Roboto, sans-serif',
                    fontSize: 40,
                    fontWeight: 'normal'
                }
            }
        }
    }

    addTextArea = () => {
        const textObject = this.state.text;
        this.props.addArea(textObject);
    };

    render(){
        return(
            <table className="canvas-toolbox">
                <thead>
                <tr>
                    <th className="toolbox-move">&nbsp;</th>
                </tr>
                <tr>
                    <th>TOOLBOX</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <IconButton
                            tooltip="Insertar texto"
                            onClick={this.addTextArea}
                        >
                            <EditorFormatSize/>
                        </IconButton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <IconButton
                            tooltip="Insertar Imágen"
                        >
                            <ImageFilter/>
                        </IconButton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <IconButton
                            tooltip="Insertar Business Object"
                        >
                            <HardwareDeveloperBoard/>
                        </IconButton>
                    </td>
                </tr>
                </tbody>
            </table>
        )
    }
}

CanvasToolbox.PropTypes = {
    addArea: PropTypes.func.isRequired
};

export default CanvasToolbox;