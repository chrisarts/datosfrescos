import React from 'react';
import PropTypes from 'prop-types';
import Upload from 'material-ui-upload/Upload';
import TextField from 'material-ui/TextField';

class ImageTool extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            area: {
                content: '',
                areaConfig: {
                    width: 0,
                    height: 0
                }
            }
        }
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            area: nextProps.area
        });
    }

    onFileLoad = (e) => {
        let area = this.state.area;
        area.content=e.target.result;
        this.props.areaUpdater(area);
    };

    onChangeWidth = (event) => {
        let area = this.state.area;
        let imagePerspective = event.target.value - area.areaConfig.width;
        area.areaConfig.height = area.areaConfig.height +(imagePerspective);
        area.areaConfig.width = event.target.value;
        this.setState({
            area
        });
        this.props.areaUpdater(area);
    };

    onChangeHeight = (event) => {
        let area = this.state.area;
        let imagePerspective = event.target.value - area.areaConfig.height;
        area.areaConfig.width = area.areaConfig.width +(imagePerspective);
        area.areaConfig.height = event.target.value;
        this.setState({
            area
        });
        this.props.areaUpdater(area);
    };

    render(){
        return(
            <div>
                <TextField
                    name="x"
                    floatingLabelText="X"
                    floatingLabelFixed={true}
                    value={parseInt(this.state.area.screenX)}
                    type="number"
                    style={{
                        width: '10%'
                    }}
                    inputStyle={{
                        textAlign: 'center'
                    }}
                    floatingLabelStyle={{
                        textAlign: 'center',
                        width: '100%'
                    }}
                    disabled={true}
                />
                <TextField
                    name="y"
                    floatingLabelText="Y"
                    floatingLabelFixed={true}
                    value={parseInt(this.state.area.screenY)}
                    type="number"
                    style={{
                        width: '10%'
                    }}
                    inputStyle={{
                        textAlign: 'center'
                    }}
                    floatingLabelStyle={{
                        textAlign: 'center',
                        width: '100%'
                    }}
                    disabled={true}
                />
                <TextField
                    name="width"
                    floatingLabelText="Ancho (PX)"
                    floatingLabelFixed={true}
                    onChange={this.onChangeWidth.bind(this)}
                    value={parseInt(this.state.area.areaConfig.width)}
                    type="number"
                    style={{
                        width: '20%'
                    }}
                    inputStyle={{
                        textAlign: 'center'
                    }}
                />
                <TextField
                    name="height"
                    floatingLabelText="Alto (PX)"
                    floatingLabelFixed={true}
                    onChange={this.onChangeHeight.bind(this)}
                    value={parseInt(this.state.area.areaConfig.height)}
                    type="number"
                    style={{
                        width: '20%'
                    }}
                    inputStyle={{
                        textAlign: 'center'
                    }}
                />
                <Upload
                    label="Subir Imágen"
                    onFileLoad={this.onFileLoad}
                />
            </div>
        )
    }
}

ImageTool.PropTypes = {
    area: PropTypes.object.isRequired,
    areaUpdater: PropTypes.func.isRequired
};

export default ImageTool;