import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

class TextTool extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            area: {
                content: '',
                areaConfig: {
                    fontSize: 0,
                    fontFamily: 0
                }
            }
        };
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            area: nextProps.area
        });
    }

    onFontSelect = (event, index, value) => {
        let area = this.state.area;
        area.areaConfig.fontFamily = value;
        this.setState({area});
        this.props.areaUpdater(this.state.area);
    };

    onChangeTextSize = (event) => {
        let area = this.state.area;
        area.areaConfig.fontSize = event.target.value;
        this.setState({area});
        this.props.areaUpdater(this.state.area);
    };

    onChangeContent = (event) => {
        let area = this.state.area;
        area.content = event.target.value;
        this.setState({area});
        this.props.areaUpdater(this.state.area);
    };

    render(){
        if(this.state.area.content === ''){
            return (<h2 className="no-tools">Cargando Información</h2>)
        }
        return(
            <div className="text-tool" style={this.props.style}>
                <h2>Opciones de Texto</h2>
                <TextField
                    name="x"
                    floatingLabelText="X"
                    floatingLabelFixed={true}
                    value={this.state.area.screenX}
                    type="number"
                    style={{
                        width: '10%'
                    }}
                    inputStyle={{
                        textAlign: 'center'
                    }}
                    floatingLabelStyle={{
                        textAlign: 'center',
                        width: '100%'
                    }}
                    disabled={true}
                />
                <TextField
                    name="y"
                    floatingLabelText="Y"
                    floatingLabelFixed={true}
                    value={this.state.area.screenY}
                    type="number"
                    style={{
                        width: '10%'
                    }}
                    inputStyle={{
                        textAlign: 'center'
                    }}
                    floatingLabelStyle={{
                        textAlign: 'center',
                        width: '100%'
                    }}
                    disabled={true}
                />
                <TextField
                    name="size"
                    floatingLabelText="Tamaño (PX)"
                    floatingLabelFixed={true}
                    onChange={this.onChangeTextSize.bind(this)}
                    value={this.state.area.areaConfig.fontSize}
                    type="number"
                    style={{
                        width: '20%'
                    }}
                    inputStyle={{
                        textAlign: 'center'
                    }}
                />
                <DropDownMenu
                    value={this.state.area.areaConfig.fontFamily}
                    onChange={this.onFontSelect}
                    style={{
                        top: 17
                    }}

                >
                    <MenuItem value={1} primaryText="Selecciona Una fuente"/>
                    <MenuItem value={`Roboto, sans-serif`} primaryText="Roboto" />
                    <MenuItem value={`Cambria, serif`} primaryText="Cambria" />
                    <MenuItem value={`Verdana, Geneva, sans-serif`} primaryText="Verdana" />
                    <MenuItem value={`Geneva`} primaryText="Geneva" />
                    <MenuItem value={`sans-serif`} primaryText="Sans Serif" />
                    <MenuItem value={`Georgia`} primaryText="Georgia" />
                    <MenuItem value={`Courier New`} primaryText="Courier New" />
                    <MenuItem value={`Arial`} primaryText="Arial" />
                    <MenuItem value={`Tahoma`} primaryText="Tahoma" />
                    <MenuItem value={`New York`} primaryText="New York" />
                </DropDownMenu>
                <TextField
                    name="text"
                    floatingLabelText="Texto"
                    floatingLabelFixed={true}
                    onChange={this.onChangeContent.bind(this)}
                    value={this.state.area.content}
                    style={{
                        width: '100%',
                    }}
                />
            </div>
        )
    }
}

TextTool.PropTypes = {
    style: PropTypes.object.isRequired,
    area: PropTypes.object.isRequired,
    areaUpdater: PropTypes.func.isRequired
};

export default TextTool;