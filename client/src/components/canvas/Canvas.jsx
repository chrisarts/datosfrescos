import React from 'react';
import {Layer, Stage, Image, Text} from 'react-konva';

import CanvasToolbox from './CanvasToolbox';
import CanvasSize from './CanvasSize';
import TextTool from './tools/TextTool';
import ImageTool from './tools/ImageTool';
import './canvas.css';

class Canvas extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            width: 725,
            height: 400,
            areas: [
                {
                    id: 0,
                    type:'text',
                    screenX: 20,
                    screenY: 30,
                    content: 'Texto de pruebas',
                    areaConfig: {
                        fontFamily: 'Roboto, sans-serif',
                        fontSize: 40,
                        fontWeight: 'normal'
                    }
                },
                {
                    id: 1,
                    type:'image',
                    screenX: 20,
                    screenY: 30,
                    content: 'https://dummyimage.com/300x200/000/fff.jpg&text=Insertar+Im%C3%A1gen',
                    areaConfig: {
                        width: 300,
                        height: 200
                    }
                },
                {
                    id: 0,
                    type:'text',
                    screenX: 20,
                    screenY: 30,
                    content: 'Texto de pruebas',
                    areaConfig: {
                        fontFamily: 'Roboto, sans-serif',
                        fontSize: 40,
                        fontWeight: 'normal'
                    }
                }
            ],
            currentAreaSelected: {},
            currentAreaSelectedIndex: 0,
            showTextTool: false,
            showImageTool: false
        };
    }

    onChange(event){
        this.setState({ [event.target.name]: event.target.value });
    }

    updateArea = (areaUpdated) => {
        let areas = this.state.areas;
        //console.log('Update area: ', areaUpdated);
        areas[this.state.currentAreaSelectedIndex] = areaUpdated;
        this.setState({
            areas
        });
    };

    dragElement = (area, areaIndex) => {
        this.setState({
            currentAreaSelected: area,
            currentAreaSelectedIndex: areaIndex
        });
        switch(area.type){
            case 'text':
                this.setState({
                    showTextTool:true,
                    showImageTool:false
                });
                break;
            case 'image':
                this.setState({
                    showTextTool:false,
                    showImageTool:true
                });
                break;
            default:
                return false;
                break;
        }
        return true;
    };

    dropElement = (area, areaIndex, event) => {
        let areas = this.state.areas;
        console.log('Before: ', areas[this.state.currentAreaSelectedIndex]);
        areas[this.state.currentAreaSelectedIndex].screenX = Math.round(event.target.attrs.x);
        areas[this.state.currentAreaSelectedIndex].screenY = Math.round(event.target.attrs.y);
        console.log('After: ',areas[this.state.currentAreaSelectedIndex]);
        this.setState({
            areas
        });
    };

    addArea = (area) => {
        let areas = this.state.areas;
        console.log(area);
        areas.push(area);
        this.setState({
            currentAreaSelected: area,
            areas
        });
    };

    render(){
        let areaIndex = 0;
        let toolToLoad = (<h2 className="no-tools">Herramientas</h2>);
        if(this.state.showTextTool){
            toolToLoad = (
                <TextTool
                    areaUpdater={this.updateArea.bind(this)}
                    style={this.state.showTextTool ? {display:'block'} : {display:'none'}}
                    area={this.state.currentAreaSelected}
                />
            )
        }
        if(this.state.showImageTool){
            toolToLoad = (
                <ImageTool
                    areaUpdater={this.updateArea.bind(this)}
                    style={this.state.showImageTool ? {display:'block'} : {display:'none'}}
                    area={this.state.currentAreaSelected}
                />
            )
        }
        return(
            <figure className="canvas-container">
                <figure>
                    <CanvasSize
                        onChange={this.onChange.bind(this)}
                        width={this.state.width}
                        height={this.state.height}
                    />
                </figure>
                <div className="area-toolbox">
                    {toolToLoad}
                </div>
                <Stage
                    width={this.state.width}
                    height={this.state.height}
                >
                    {
                        this.state.areas.map(area => {
                            let drawer = (
                                <div key={-1}>
                                    <h2>Zona Herramientas</h2>
                                </div>
                            );
                            switch(area.type){
                                case 'text':
                                    drawer = (
                                        <Layer key={areaIndex}>
                                            <Text
                                                x={area.screenX} y={area.screenY}
                                                text={area.content}
                                                draggable="true"
                                                onDragStart={this.dragElement.bind(this, area, areaIndex)}
                                                onDragEnd={this.dropElement.bind(this, area, areaIndex)}
                                                onClick={this.dragElement.bind(this, area, areaIndex)}
                                                {...area.areaConfig}
                                            />
                                        </Layer>
                                    );
                                    break;
                                case 'image':
                                    const image = new window.Image();
                                    image.src = area.content;
                                    drawer = (
                                        <Layer key={areaIndex}>
                                            <Image
                                                x={area.screenX} y={area.screenY}
                                                width={area.areaConfig.width}
                                                height={area.areaConfig.height}
                                                image={image}
                                                draggable="true"
                                                onDragStart={this.dragElement.bind(this, area, areaIndex)}
                                                onDragEnd={this.dropElement.bind(this, area, areaIndex)}
                                                onClick={this.dragElement.bind(this, area, areaIndex)}
                                                {...area.areaConfig}
                                            />
                                        </Layer>
                                    );
                                    break;
                                default:
                                    return drawer;
                                    break;
                            }
                            areaIndex++;
                            return drawer;
                        })
                    }
                </Stage>
                <CanvasToolbox
                    addArea={this.addArea.bind(this)}
                />
            </figure>
        )
    }
}

export default Canvas;