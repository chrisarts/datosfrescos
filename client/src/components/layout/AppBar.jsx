import React from 'react';
import {connect} from 'react-redux';
import {Link, browserHistory} from 'react-router';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';
import {
    ActionAccountCircle
} from 'material-ui/svg-icons';


import {logout,login} from '../../redux/actions/authActions';
import './appBar.css';

class AppToolbar extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            toolbarStyle: {
                backgroundColor: 'rgba(0,0,0,0.8)'
            },
            currentRoute: '/',
            isAuth: false,
            basketCount: 0,
            user: {},
            menuPadding: false
        };
    }

    componentDidMount(){
        let This = this;
    }

    componentWillUnmount(){
        window.removeEventListener('scroll', this.handleScroll.bind(this));
    }

    handleScroll(event){
        let newToolbarStyle = this.state.toolbarStyle;
        if(event.srcElement.body.scrollTop > 150){
            newToolbarStyle.backgroundColor = '#1a1a1a';
            newToolbarStyle.height = 70;
            this.setState({
                toolbarStyle: newToolbarStyle,
                menuPadding: true
            });
        }else{
            newToolbarStyle.backgroundColor = 'rgba(0,0,0,0.5)';
            newToolbarStyle.height = 56;
            this.setState({
                toolbarStyle: newToolbarStyle,
                menuPadding: false
            });
        }
    }

    routeChange(e){
        e.preventDefault();
        let linkTo = e.target.parentNode.getAttribute('href');
        this.setState({
            currentRoute: linkTo
        });
        this.context.router.push(linkTo);
    }

    render(){
        return(
            <div>
                <Toolbar
                    className={`app-toolbar`}
                    style={this.state.toolbarStyle}
                >
                    <ToolbarGroup firstChild={true}>
                        <figure className="page-logo">
                            <img
                                style={{height: '100%'}}
                                src={`/images/logo.png`} alt="Datosfrescos"/>
                        </figure>
                    </ToolbarGroup>
                    <ToolbarGroup>
                        <div
                            className={`main-menu`}
                        >
                            <Link
                                to="/"
                                className={"menu-link"}
                                activeClassName="active"
                                onClick={this.routeChange.bind(this)}
                            >
                                <span>Home</span>
                            </Link>
                            <Link
                                to="/plantillas"
                                className={"menu-link"}
                                activeClassName="active"
                                onClick={this.routeChange.bind(this)}
                            >
                                <span>Plantillas</span>
                            </Link>
                        </div>
                    </ToolbarGroup>
                    <ToolbarGroup
                        style={{
                            width: '18%'
                        }}
                    >
                        <div className="user-menu">
                            <div className="profile-menu">
                                {
                                    this.state.isAuth ? (
                                        <IconMenu
                                            iconButtonElement={
                                                <IconButton>
                                                    <ActionAccountCircle/>
                                                </IconButton>
                                            }
                                            style={{position:'absolute'}}
                                            anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                                            className={(this.state.menuPadding ? "icon-profile-menu-padding" : "icon-profile-menu")}
                                        >
                                            <MenuItem primaryText="Ver Perfil" />
                                        </IconMenu>
                                    ) : (
                                        <div className="signup-menu">
                                            <Link
                                                to="/registrate"
                                                onChange={this.routeChange.bind(this)}
                                                className={"menu-link"}
                                            >
                                                <span>Registrate!</span>
                                            </Link>
                                            <Link
                                                to="/iniciar-sesion"
                                                onChange={this.routeChange.bind(this)}
                                                className={"menu-link"}
                                            >
                                                <span>Iniciar sesión!</span>
                                            </Link>
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                    </ToolbarGroup>
                </Toolbar>
            </div>
        )
    }
}

AppToolbar.PropTypes = {
    auth: React.PropTypes.object.isRequired,
    logout: React.PropTypes.func.isRequired
};

AppToolbar.contextTypes = {
    router: React.PropTypes.object.isRequired
};

function mapStateToProps(state){
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps, {logout})(AppToolbar);