import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import AppBar from './layout/AppBar';
import './styles.css';

class App extends React.Component{

    render(){
        return(
            <MuiThemeProvider>
                <div>
                    <AppBar/>
                    {this.props.children}
                </div>
            </MuiThemeProvider>
        )
    }
}

export default App;

