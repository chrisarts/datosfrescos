import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

import './login.css';

class Login extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            user: '',
            password: '',
            isLoading: false
        };
    }

    onChange(event){
        this.setState({ [event.target.name]: event.target.value })
    }

    onSubmit(){
        console.log('Submit login: ', this.state);
    }

    render(){
        return(
            <div className="page login">
                <form onSubmit={this.onSubmit.bind(this)}>
                    <TextField
                        name="user"
                        hintText="Correo Electrónico"
                        floatingLabelText="Ingresa tu Correo *"
                        value={this.state.user}
                        onChange={this.onChange.bind(this)}
                    />
                    <TextField
                        name="user"
                        hintText="Correo Electrónico"
                        floatingLabelText="Ingresa tu Correo *"
                        value={this.state.user}
                        onChange={this.onChange.bind(this)}
                    />
                    <FlatButton
                        label="Ingresar"
                        type="submit"
                    />
                </form>
            </div>
        )
    }
}

export default connect(null, {})(Login);