import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

// Fitmas Page Components
import App from './components/App';
import Home from './components/Home';
import Login from './components/login/Login';

//Routes for this app (if any route must be protected wrap it with the function requireAuth(ReactComponent)
export default (
    <Router history={browserHistory}>
        <Route path="/" components={App}>
            <IndexRoute component={Home} />
            <Route path="/login" component={Login} />
        </Route>
    </Router>

);
