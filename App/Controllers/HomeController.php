<?php

namespace App\Controllers\HomeController;

use App\Controller\Controller;
use App\Core\Route\Route;

class HomeController extends Controller{
    function __construct(Route $route){
        //Define API Routes
        $route::get('/api/companies', function(){
            self::setApiHeaders();
            return self::get($_GET);
        }, array('requireAuth'=>false));
        $route::get('/api/companies/nit', function(){
            self::setApiHeaders();
            return self::getByNIT($_GET);
        }, array('requireAuth'=>false));
    }

    public static function get($get){
        try{
            if(!is_array($get) || !isset($get['id'])){
                return array(
                    'error'=>true,
                    'message'=>'No se ha especificado ninguna empresa'
                );
            }
            return Company::find($get['id']);
        }catch (\Exception $e){
            return array('error'=>true,'message'=>$e->getMessage());
        }
    }

    public static function getAll(){
        header('Content-type: application/json');
        self::query("SELECT nombre,id FROM empresas");
    }

    public static function getByNIT($get){
        try{
            $dependencies = [];
            $company = Company::where('documento',$get['document'])->first();
            if($company!==NULL){
                $dependencies = self::getDependencies($company->id);
            }else{
                return array('error'=>true, 'message'=>'La empresa no existe', 'dependencies'=>$dependencies);
            }
            return array('error'=>false, 'company'=>$company, 'dependencies'=>$dependencies);
        }catch (\Exception $e){
            return array('error'=>true, 'message'=>$e->getMessage(), 'errorCode'=>$e->getCode());
        }
    }

    public static function getDependencies($company){
        try{
            $company = intval($company);
            return Dependency::where([
                ['id_empresa',$company],
                ['activo',1]
            ])->get()->toArray();
        }catch (\Exception $e){
            return array('error'=>true, 'message'=>$e->getMessage(), 'errorCode'=>$e->getCode());
        }
    }
}