<?php
namespace App\Tables;

use Illuminate\Database\Eloquent\Model;

class Area extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'areas';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}