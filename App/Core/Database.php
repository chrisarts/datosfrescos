<?php
namespace App\Core;

use Illuminate\Database\Capsule\Manager as DbHandler;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

//Database Driver
use PDO;

/**
 * Class Database
 * @package App\Core\Database
 */
class Database {

    private static $host = "localhost";
    private static $dbName = "logicalsignage_dev";
    private static $user = "root";
    private static $pass = "developer8";


    function __construct(){
        // Create Capsule for a new Database instance
        try{
            $dbHandler = new DbHandler();
            $dbHandler->addConnection([
                'driver'    => 'pgsql',
                'host'      => self::$host,
                'database'  => self::$dbName,
                'username'  => self::$user,
                'password'  => self::$pass,
                'charset'   => 'utf8',
                'collation' => 'utf8_spanish_ci',
                'prefix'    => '',
            ]);
            // Set the event dispatcher used by Eloquent models... (optional)
            $dbHandler->setEventDispatcher(new Dispatcher(new Container));
            // Make this Capsule instance available globally via static methods... (optional)
            $dbHandler->setAsGlobal();
            // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
            $dbHandler->bootEloquent();
        }catch (\Exception $e){
            echo json_encode(array('error'=>true, 'message'=>$e->getMessage(), 'errorCode'=>$e->getCode()));
            die();
        }
    }
}