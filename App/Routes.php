<?php
namespace App\Routes;

use App\Core\Route\Route;

use App\Controllers\HomeController\HomeController;

//Create new Route Handler
$route = new Route();

//App Routes, if you want to create a new route please use relative paths (like /route/to/create)

$route::get('/', function(){
    echo HomeController::CreateView('Index');
}, array('requireAuth' => false));

$route::render();